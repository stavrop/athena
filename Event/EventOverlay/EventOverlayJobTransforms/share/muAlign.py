
haas_mualign2=True
if haas_mualign2:
    #use muon alignments
    print "Haas: Reading muon alignment constants from DB"

    from AthenaCommon.AlgSequence import AthSequencer

    #from IOVDbSvc.CondDB import conddb
    #conddb.addFolderSplitOnline('MUONALIGN','/MUONALIGN/Onl/MDT/BARREL','/MUONALIGN/MDT/BARREL')
    #conddb.addFolderSplitOnline('MUONALIGN','/MUONALIGN/Onl/MDT/ENDCAP/SIDEA','/MUONALIGN/MDT/ENDCAP/SIDEA')
    #conddb.addFolderSplitOnline('MUONALIGN','/MUONALIGN/Onl/MDT/ENDCAP/SIDEC','/MUONALIGN/MDT/ENDCAP/SIDEC')
    #conddb.addFolderSplitOnline('MUONALIGN','/MUONALIGN/Onl/TGC/SIDEA','/MUONALIGN/TGC/SIDEA')
    #conddb.addFolderSplitOnline('MUONALIGN','/MUONALIGN/Onl/TGC/SIDEC','/MUONALIGN/TGC/SIDEC')

    from AtlasGeoModel.MuonGM import GeoModelSvc
    MuonDetectorTool = GeoModelSvc.DetectorTools[ "MuonDetectorTool" ]
    MuonDetectorTool.UseConditionDb = 1
    MuonDetectorTool.OutputLevel=DEBUG
    
    MuonDetectorTool.EnableFineClashFixing = 1 #this should be on for g4?
    print MuonDetectorTool

    #register callbacks for alignments, to get IOVs?
    #GeoModelSvc.AlignCallbacks = True
    
    from MuonCondAlg.MuonCondAlgConf import MuonAlignmentDbAlg
    condSequence = AthSequencer("AthCondSeq")
    if conddb.dbdata != 'COMP200' and conddb.dbmc != 'COMP200' and \
       'HLT' not in globalflags.ConditionsTag() and not conddb.isOnline :

        condSequence+=MuonAlignmentDbAlg('MuonAlignmentDbAlg')
        MuonAlignmentDbAlg.ParlineFolders = ["/MUONALIGN/MDT/BARREL",
                                             "/MUONALIGN/MDT/ENDCAP/SIDEA",
                                             "/MUONALIGN/MDT/ENDCAP/SIDEC",
                                             "/MUONALIGN/TGC/SIDEA",
                                             "/MUONALIGN/TGC/SIDEC"]
        MuonAlignmentDbAlg.ILinesFromCondDB = False
        MuonAlignmentDbAlg.DumpALines = False
        MuonAlignmentDbAlg.DumpBLines = False
        MuonAlignmentDbAlg.DumpILines = False
    

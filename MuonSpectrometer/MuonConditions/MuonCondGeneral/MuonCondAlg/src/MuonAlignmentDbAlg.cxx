/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#include "GaudiKernel/MsgStream.h"

#include "StoreGate/StoreGateSvc.h"
#include "SGTools/TransientAddress.h"
#include "CoralBase/Attribute.h"
#include "CoralBase/AttributeListSpecification.h"
#include "AthenaPoolUtilities/AthenaAttributeList.h"
#include "AthenaPoolUtilities/CondAttrListCollection.h"

#include "MuonReadoutGeometry/MuonDetectorManager.h"
#include "MuonReadoutGeometry/GlobalUtilities.h"

#include "Identifier/IdentifierHash.h"
#include "Identifier/Identifier.h"
#include "MuonIdHelpers/MdtIdHelper.h"
#include "MuonIdHelpers/CscIdHelper.h"
#include "MuonIdHelpers/TgcIdHelper.h"
#include "MuonIdHelpers/RpcIdHelper.h"
#include "PathResolver/PathResolver.h"
#include <fstream>
#include <string>
#include <map>

#include "MuonCondAlg/MuonAlignmentDbAlg.h"

#include "MuonCondSvc/MdtStringUtils.h"

MuonAlignmentDbAlg::MuonAlignmentDbAlg(const std::string& name, 
				       ISvcLocator* pSvcLocator) 
  : AthAlgorithm(name, pSvcLocator),
    m_condSvc{"CondSvc", name}
{
  m_mdtIdHelper = 0;
  m_cscIdHelper = 0;
  m_rpcIdHelper = 0;
  m_tgcIdHelper = 0;
  m_geometryVersion = "";

  m_parlineFolder.clear();
  declareProperty("ParlineFolders",  m_parlineFolder);
  declareProperty("DumpALines",      m_dumpALines=false);
  declareProperty("DumpBLines",      m_dumpBLines=false);
  declareProperty("DumpILines",      m_dumpILines=false);
  declareProperty("ILinesFromCondDB",m_ILinesFromDb=false);
  declareProperty("ALinesFile",      m_aLinesFile="");
  declareProperty("AsBuiltFile",     m_asBuiltFile="");
}

StatusCode MuonAlignmentDbAlg::initialize(){

  ATH_MSG_INFO("Initilalizing");
  ATH_MSG_INFO("In initialize ---- # of folders registered is " << m_parlineFolder.size());

  ATH_CHECK(m_condSvc.retrieve());

  // Read Handles Keys
  ATH_CHECK(m_readMdtBarrelKey.initialize());
  ATH_CHECK(m_readMdtEndcapSideAKey.initialize());
  ATH_CHECK(m_readMdtEndcapSideCKey.initialize());
  ATH_CHECK(m_readTgcSideAKey.initialize());
  ATH_CHECK(m_readTgcSideCKey.initialize());
  ATH_CHECK(m_readCscILinesKey.initialize());
  ATH_CHECK(m_readMdtAsBuiltParamsKey.initialize());

  // Write Handles
  ATH_CHECK(m_writeALineKey.initialize());
  if(m_condSvc->regHandle(this, m_writeALineKey).isFailure()) {
    ATH_MSG_FATAL("unable to register WriteCondHandle " << m_writeALineKey.fullKey() << " with CondSvc");
    return StatusCode::FAILURE;
  }
  ATH_CHECK(m_writeBLineKey.initialize());
  if(m_condSvc->regHandle(this, m_writeBLineKey).isFailure()) {
    ATH_MSG_FATAL("unable to register WriteCondHandle " << m_writeBLineKey.fullKey() << " with CondSvc");
    return StatusCode::FAILURE;
  }
  ATH_CHECK(m_writeILineKey.initialize());
  if(m_condSvc->regHandle(this, m_writeILineKey).isFailure()) {
    ATH_MSG_FATAL("unable to register WriteCondHandle " << m_writeILineKey.fullKey() << " with CondSvc");
    return StatusCode::FAILURE;
  }
  ATH_CHECK(m_writeAsBuiltKey.initialize());
  if(m_condSvc->regHandle(this, m_writeAsBuiltKey).isFailure()) {
    ATH_MSG_FATAL("unable to register WriteCondHandle " << m_writeAsBuiltKey.fullKey() << " with CondSvc");
    return StatusCode::FAILURE;
  }
    
  //=================
  // Initialize geometry and Id Helpers. Initialization of the pointer to the MuonDetectorManager.
  // !!!!!!!!!!! It was called in the loadParameters before. !!!!!!!!!!
  //=================

  if (InitializeGeometryAndIdHelpers().isFailure()) {
    ATH_MSG_FATAL("Error in InitializeGeometryAndIdHelpers");
    return StatusCode::FAILURE;
  }
 

 return StatusCode::SUCCESS;
}

StatusCode MuonAlignmentDbAlg::execute(){
  
  StatusCode sc = StatusCode::SUCCESS;

  ATH_MSG_DEBUG( "execute " << name() ); 

  sc = loadParameters();

  return sc;
}


StatusCode MuonAlignmentDbAlg::finalize(){
  
  ATH_MSG_DEBUG( "finalize " << name() );
  return StatusCode::SUCCESS;
}

StatusCode MuonAlignmentDbAlg::InitializeGeometryAndIdHelpers(){

  //=================
  // Initialize pointer to the MuonDetectorManager
  //=================
  
  if (StatusCode::SUCCESS != detStore()->retrieve(m_muonMgr)) {
    ATH_MSG_FATAL("Couldn't load MuonDetectorManager");
    return StatusCode::FAILURE;
  }

  //=================
  // Initialize geometry
  //=================

  m_geometryVersion = m_muonMgr->geometryVersion();
  ATH_MSG_INFO("geometry version from the MuonDetectorManager = " << m_geometryVersion);
  
  //=================
  // Initialize Helpers
  //=================
  
  if (m_mdtIdHelper == 0) {
    if (detStore()->retrieve(m_mdtIdHelper, "MDTIDHELPER" ).isFailure()) {
      ATH_MSG_ERROR("Can't retrieve MdtIdHelper");
      return StatusCode::FAILURE;
    } else ATH_MSG_INFO("MdtIdHelper retrieved from the DetectorStore");
  } else ATH_MSG_DEBUG("MdtIdHelper already initialized");

  //-------------------

  if (m_cscIdHelper == 0) {
    if (detStore()->retrieve(m_cscIdHelper, "CSCIDHELPER" ).isFailure()) {
      ATH_MSG_ERROR("Can't retrieve CscIdHelper");
      return StatusCode::FAILURE;
    } else ATH_MSG_INFO("CscIdHelper retrieved from the DetectorStore");
  } else ATH_MSG_DEBUG("CscIdHelper already initialized");

  //-------------------

  if (m_rpcIdHelper == 0) {
    if (detStore()->retrieve(m_rpcIdHelper, "RPCIDHELPER" ).isFailure()) {
      ATH_MSG_ERROR("Can't retrieve RpcIdHelper");
      return StatusCode::FAILURE;
    } else ATH_MSG_INFO("RpcIdHelper retrieved from the DetectorStore");
  } else ATH_MSG_DEBUG("RpcIdHelper already initialized");

  //-------------------

  if (m_tgcIdHelper == 0) {
    if (detStore()->retrieve(m_tgcIdHelper, "TGCIDHELPER" ).isFailure()) {
      ATH_MSG_ERROR("Can't retrieve TgcIdHelper");
      return StatusCode::FAILURE;
    } else ATH_MSG_INFO("TgcIdHelper retrieved from the DetectorStore");
  } else ATH_MSG_DEBUG("TgcIdHelper already initialized");

  //-------------------

  // !!!!! HERE WE SHOULD PUT CODE FOR NSW (MM and STGC)

  return StatusCode::SUCCESS;
}


StatusCode MuonAlignmentDbAlg::loadParameters() {

  StatusCode sc = StatusCode::SUCCESS;

  ATH_MSG_DEBUG( "In LoadParameters " << name() );

  // =======================
  // Loop on folders requested in configuration and check if /MUONALIGN/CSC/ILINES and /MUONALIGN/MDT/ASBUILTPARAMS are requested
  // =======================
  bool AsBuiltRequested = false;
  bool ILineRequested = false;
  for (std::vector<std::string>::const_iterator itf=m_parlineFolder.begin(); 
       itf!=m_parlineFolder.end(); ++itf ) {
    std::string currentFolderName = *itf;
    if(currentFolderName.find("ILINES") != std::string::npos) ILineRequested = true;
    if(currentFolderName.find("ASBUILTPARAMS") != std::string::npos) AsBuiltRequested = true;
  }

  // =======================
  // Load ILine parameters if requested in the joboptions and /MUONALIGN/CSC/ILINES folder given in the joboptions
  // =======================
  if( m_ILinesFromDb and ILineRequested) {
    sc = loadAlignILines("/MUONALIGN/CSC/ILINES");
  } 
  else if (ILineRequested and not m_ILinesFromDb) {
    ATH_MSG_INFO("DB folder for I-Lines given in job options however loading disabled ");
    sc = StatusCode::SUCCESS;
  }
  if(sc.isFailure()) return StatusCode::FAILURE;

  // =======================
  // Load AsBuilt parameters if /MUONALIGN/MDT/ASBUILTPARAMS folder given in the joboptions
  // =======================
  if(AsBuiltRequested) sc = loadAlignAsBuilt("/MUONALIGN/MDT/ASBUILTPARAMS");
  if(sc.isFailure()) return StatusCode::FAILURE;

  // =======================
  // Load A- and B-Lines
  // =======================
  sc = loadAlignABLines();
  if(sc.isFailure()) return StatusCode::FAILURE;

  return sc;
}

StatusCode MuonAlignmentDbAlg::loadAlignABLines() {

  // =======================
  // Write ALine Cond Handle
  // =======================
  SG::WriteCondHandle<ALineMapContainer> writeALineHandle{m_writeALineKey};
  if (writeALineHandle.isValid()) {
    ATH_MSG_DEBUG("CondHandle " << writeALineHandle.fullKey() << " is already valid."
		  << ". In theory this should not be called, but may happen"
		  << " if multiple concurrent events are being processed out of order.");
    return StatusCode::SUCCESS;
  }
  std::unique_ptr<ALineMapContainer> writeALineCdo{std::make_unique<ALineMapContainer>()};

  // =======================
  // Write BLine Cond Handle
  // =======================
  SG::WriteCondHandle<BLineMapContainer> writeBLineHandle{m_writeBLineKey};
  if (writeBLineHandle.isValid()) {
    ATH_MSG_DEBUG("CondHandle " << writeBLineHandle.fullKey() << " is already valid."
		  << ". In theory this should not be called, but may happen"
		  << " if multiple concurrent events are being processed out of order.");
    return StatusCode::SUCCESS;
  }
  std::unique_ptr<BLineMapContainer> writeBLineCdo{std::make_unique<BLineMapContainer>()};


  // =======================
  // Loop on folders requested in configuration and process the ones that have A- and B-lines
  // =======================

  EventIDRange rangeALineW;
  EventIDRange rangeBLineW;
  for (std::vector<std::string>::const_iterator itf=m_parlineFolder.begin(); 
       itf!=m_parlineFolder.end(); ++itf ) {
    std::string currentFolderName = *itf;
    if(currentFolderName.find("ILINES") != std::string::npos) continue;
    if(currentFolderName.find("ASBUILTPARAMS") != std::string::npos) continue;
    // Process the current folder for the A- and B-lines
    if(loadAlignABLines(currentFolderName, 
			writeALineCdo.get(), 
			writeBLineCdo.get(),
			rangeALineW,
			rangeBLineW).isSuccess()) {
      ATH_MSG_INFO("A- and B-Lines parameters from DB folder <" << currentFolderName << "> loaded");
    } else {
      ATH_MSG_ERROR("A- and B-Lines parameters from DB folder <" << currentFolderName << "> failed to load");
      return StatusCode::FAILURE; 
    }
  }

  // =======================
  // FIRST Update the MuonDetectorManager and THEN record the ALine.
  // =======================

  if (m_muonMgr->updateAlignment(writeALineCdo.get()).isFailure()) ATH_MSG_ERROR("Unable to updateAlignment" );
  else ATH_MSG_DEBUG("updateAlignment DONE" );

  if (writeALineHandle.record(rangeALineW, std::move(writeALineCdo)).isFailure()) {
    ATH_MSG_FATAL("Could not record ALineMapContainer " << writeALineHandle.key() 
		  << " with EventRange " << rangeALineW
		  << " into Conditions Store");
    return StatusCode::FAILURE;
  }		  
  ATH_MSG_INFO("recorded new " << writeALineHandle.key() << " with range " << rangeALineW << " into Conditions Store");

  // =======================
  // FIRST Update the MuonDetectorManager and THEN record the BLine.
  // =======================

  if (m_muonMgr->updateDeformations(writeBLineCdo.get()).isFailure()) ATH_MSG_ERROR("Unable to updateDeformations" );
  else ATH_MSG_DEBUG("updateDeformations DONE" );

  if (writeBLineHandle.record(rangeBLineW, std::move(writeBLineCdo)).isFailure()) {
    ATH_MSG_FATAL("Could not record BLineMapContainer " << writeBLineHandle.key() 
		  << " with EventRange " << rangeBLineW
		  << " into Conditions Store");
    return StatusCode::FAILURE;
  }		  
  ATH_MSG_INFO("recorded new " << writeBLineHandle.key() << " with range " << rangeBLineW << " into Conditions Store");

  return StatusCode::SUCCESS;
}

StatusCode MuonAlignmentDbAlg::loadAlignABLines(std::string folderName,
						ALineMapContainer* writeALineCdo,
						BLineMapContainer* writeBLineCdo,
						EventIDRange& rangeALineW,
						EventIDRange& rangeBLineW) {

  ATH_MSG_INFO("Load alignment parameters from DB folder <" << folderName << ">");

  bool hasBLine = true;
  if (folderName.find("TGC")!=std::string::npos)  {
    hasBLine = false;
    ATH_MSG_INFO("No BLines decoding will be attempted for folder named " << folderName);
  }
   
  // =======================
  // Read Cond Handles and ranges for current folder
  // =======================
  EventIDRange rangeALinesTemp;
  EventIDRange rangeBLinesTemp;
  if (folderName == "/MUONALIGN/MDT/BARREL") {

    SG::ReadCondHandle<CondAttrListCollection> readHandle{m_readMdtBarrelKey};
    const CondAttrListCollection* readCdo{*readHandle}; 
    if(readCdo==nullptr){
      ATH_MSG_ERROR("Null pointer to the read ALINES conditions object");
      return StatusCode::FAILURE; 
    } 
    
    if ( !readHandle.range(rangeALinesTemp) ) {
      ATH_MSG_ERROR("Failed to retrieve ALines validity range for " << readHandle.key());
      return StatusCode::FAILURE;
    }
    if ( !readHandle.range(rangeBLinesTemp) ) {
      ATH_MSG_ERROR("Failed to retrieve BLines validity range for " << readHandle.key());
      return StatusCode::FAILURE;
    } 
    
    ATH_MSG_INFO("Size of /MUONALIGN/MDT/BARREL CondAttrListCollection " << readHandle.fullKey() << " readCdo->size()= " << readCdo->size());
    ATH_MSG_INFO("Range of /MUONALIGN/MDT/BARREL input is, ALines: " << rangeALinesTemp << " BLines: " << rangeBLinesTemp);

  } else if (folderName == "/MUONALIGN/MDT/ENDCAP/SIDEA") {

    SG::ReadCondHandle<CondAttrListCollection> readHandle{m_readMdtEndcapSideAKey};
    const CondAttrListCollection* readCdo{*readHandle}; 
    if(readCdo==nullptr){
      ATH_MSG_ERROR("Null pointer to the read ALINES conditions object");
      return StatusCode::FAILURE; 
    } 
    
    if ( !readHandle.range(rangeALinesTemp) ) {
      ATH_MSG_ERROR("Failed to retrieve ALines validity range for " << readHandle.key());
      return StatusCode::FAILURE;
    } 
    if ( !readHandle.range(rangeBLinesTemp) ) {
      ATH_MSG_ERROR("Failed to retrieve BLines validity range for " << readHandle.key());
      return StatusCode::FAILURE;
    } 
    
    ATH_MSG_INFO("Size of /MUONALIGN/MDT/ENDCAP/SIDEA CondAttrListCollection " << readHandle.fullKey() << " readCdo->size()= " << readCdo->size());
    ATH_MSG_INFO("Range of /MUONALIGN/MDT/ENDCAP/SIDEA input is, ALines: " << rangeALinesTemp << " BLines: " << rangeBLinesTemp);

  } else if (folderName == "/MUONALIGN/MDT/ENDCAP/SIDEC") {

    SG::ReadCondHandle<CondAttrListCollection> readHandle{m_readMdtEndcapSideCKey};
    const CondAttrListCollection* readCdo{*readHandle}; 
    if(readCdo==nullptr){
      ATH_MSG_ERROR("Null pointer to the read ALINES conditions object");
      return StatusCode::FAILURE; 
    } 
    
    if ( !readHandle.range(rangeALinesTemp) ) {
      ATH_MSG_ERROR("Failed to retrieve ALines validity range for " << readHandle.key());
      return StatusCode::FAILURE;
    } 
    if ( !readHandle.range(rangeBLinesTemp) ) {
      ATH_MSG_ERROR("Failed to retrieve BLines validity range for " << readHandle.key());
      return StatusCode::FAILURE;
    } 
    
    ATH_MSG_INFO("Size of /MUONALIGN/MDT/ENDCAP/SIDEC CondAttrListCollection " << readHandle.fullKey() << " readCdo->size()= " << readCdo->size());
    ATH_MSG_INFO("Range of /MUONALIGN/MDT/ENDCAP/SIDEC input is, ALines: " << rangeALinesTemp << " BLines: " << rangeBLinesTemp);

  } else if (folderName == "/MUONALIGN/TGC/SIDEA") {

    SG::ReadCondHandle<CondAttrListCollection> readHandle{m_readTgcSideAKey};
    const CondAttrListCollection* readCdo{*readHandle}; 
    if(readCdo==nullptr){
      ATH_MSG_ERROR("Null pointer to the read ALINES conditions object");
      return StatusCode::FAILURE; 
    } 
    
    if ( !readHandle.range(rangeALinesTemp) ) {
      ATH_MSG_ERROR("Failed to retrieve ALines validity range for " << readHandle.key());
      return StatusCode::FAILURE;
    } 

    // !!!!!!!! NO BLINES FOR TGCs !!!!!!!!!!!!!!
    
    ATH_MSG_INFO("Size of /MUONALIGN/TGC/SIDEA CondAttrListCollection " << readHandle.fullKey() << " readCdo->size()= " << readCdo->size());
    ATH_MSG_INFO("Range of /MUONALIGN/TGC/SIDEA input is, ALines: " << rangeALinesTemp);

  } else if (folderName == "/MUONALIGN/TGC/SIDEC") {

    SG::ReadCondHandle<CondAttrListCollection> readHandle{m_readTgcSideCKey};
    const CondAttrListCollection* readCdo{*readHandle}; 
    if(readCdo==nullptr){
      ATH_MSG_ERROR("Null pointer to the read ALINES conditions object");
      return StatusCode::FAILURE; 
    } 
    
    if ( !readHandle.range(rangeALinesTemp) ) {
      ATH_MSG_ERROR("Failed to retrieve ALines validity range for " << readHandle.key());
      return StatusCode::FAILURE;
    } 
    // !!!!!!!! NO BLINES FOR TGCs !!!!!!!!!!!!!!
    
    ATH_MSG_INFO("Size of /MUONALIGN/TGC/SIDEC CondAttrListCollection " << readHandle.fullKey() << " readCdo->size()= " << readCdo->size());
    ATH_MSG_INFO("Range of /MUONALIGN/TGC/SIDEC input is, ALines: " << rangeALinesTemp);

  }

  // Combine the validity ranges for ALines
  EventIDRange rangeIntersection{EventIDRange::intersect(rangeALineW, rangeALinesTemp)};     
  ATH_MSG_DEBUG("rangeIntersection: " << rangeIntersection << " Previous range: " << rangeALineW << " Current range : " << rangeALinesTemp);
  if (rangeIntersection.stop().isValid() and rangeIntersection.start()>rangeIntersection.stop()) {       
    ATH_MSG_FATAL("Invalid intersection range: " << rangeIntersection);       
    return StatusCode::FAILURE;     }     
  rangeALineW = rangeIntersection;

  // Combine the validity ranges for BLines
  if (hasBLine) {
    EventIDRange rangeIntersection{EventIDRange::intersect(rangeBLineW, rangeBLinesTemp)};     
    ATH_MSG_DEBUG("rangeIntersection: " << rangeIntersection << " Previous range: " << rangeALineW << " Current range: " << rangeALinesTemp);
    if (rangeIntersection.stop().isValid() and rangeIntersection.start()>rangeIntersection.stop()) {       
      ATH_MSG_FATAL("Invalid intersection range: " << rangeIntersection);       
      return StatusCode::FAILURE;     }     
    rangeBLineW = rangeIntersection;  
  }

  // =======================
  // retreive the collection of strings read out from the DB
  // =======================
  const CondAttrListCollection * atrc;
  if(detStore()->retrieve(atrc,folderName).isFailure())  {
    ATH_MSG_WARNING("could not retreive the CondAttrListCollection from DB folder " << folderName);
    return StatusCode::RECOVERABLE;
  }
  else
    ATH_MSG_INFO(" CondAttrListCollection from DB folder have been obtained with size " << atrc->size());

  // unpack the strings in the collection and update the 
  // ALlineContainer in TDS
  int nLines = 0;
  int nDecodedLines = 0;
  int nNewDecodedALines = 0;
  int nNewDecodedBLines = 0;
  CondAttrListCollection::const_iterator itr;
  for (itr = atrc->begin(); itr != atrc->end(); ++itr) {
    const coral::AttributeList& atr=itr->second;
    std::string data;
    data=*(static_cast<const std::string*>((atr["data"]).addressOfData()));
    
    ATH_MSG_DEBUG("Data load is " << data << " FINISHED HERE ");
  
    // Check the first word to see if it is a correction
    std::string type;
    
    //Parse corrections
    std::string since_str;
    std::string till_str;
    std::string delimiter = "\n";
      
    
    std::vector<std::string> lines;
    MuonCalib::MdtStringUtils::tokenize(data,lines,delimiter);
    for (unsigned int i=0; i<lines.size();i++) {
      ++nLines;

      std::string blobline = lines[i];
      
      std::string delimiter = ":";
      std::vector<std::string> tokens;
      MuonCalib::MdtStringUtils::tokenize(blobline,tokens,delimiter);
      type = tokens[0];
      //Parse line
      if (type.find("#")==0) {
	//skip it
	continue;
      }

      if (type.find("Header")==0) {
	std::string delimiter = "|";
	std::vector<std::string> tokens;
	MuonCalib::MdtStringUtils::tokenize(blobline,tokens,delimiter);
	since_str = tokens[1];
	till_str = tokens[2];
      }
	
      if (type.find("IOV")==0) {
	std::string delimiter = " ";
	std::vector<std::string> tokens;
	MuonCalib::MdtStringUtils::tokenize(blobline,tokens,delimiter);
	int ival = 1;
	long int iovThisBlob=0;
	// long int lastIOV = getLastIOVforThisFolder(folderName);
	
	std::string str_iovThisBlob = tokens[ival];
	sscanf(str_iovThisBlob.c_str(),"%80ld",&iovThisBlob);
	ATH_MSG_INFO("Data read from folder " << folderName <<
		     " have IoV = " << iovThisBlob);
      }
	
      if (type.find("Corr")==0) {
//#: Corr line is counter typ,  jff,  jzz, job,                         * Chamber information 
//#:                       svalue,  zvalue, tvalue,  tsv,  tzv,  ttv,   * A lines 
//#:                       bz, bp, bn, sp, sn, tw, pg, tr, eg, ep, en   * B lines 
//#:                       chamber                                      * Chamber name 
//.... example
//Corr: EMS  4   1  0     2.260     3.461    28.639 -0.002402 -0.002013  0.000482    -0.006    -0.013 -0.006000  0.000000  0.000000     0.026    -0.353  0.000000  0.070000  0.012000    -0.012    EMS1A08

	std::string delimiter = " ";
	std::vector<std::string> tokens;
	MuonCalib::MdtStringUtils::tokenize(blobline,tokens,delimiter);

	ATH_MSG_VERBOSE("Parsing Line = ");
	for (unsigned int i=0; i<tokens.size(); i++) ATH_MSG_VERBOSE( tokens[i] << " | ");
	ATH_MSG_VERBOSE( " " );

	bool thisRowHasBLine = true;
	if (tokens.size()<15) {
	  // only A-lines ..... old COOL blob convention for barrel 
	  thisRowHasBLine = false;
	  ATH_MSG_VERBOSE("(old COOL blob convention for barrel) skipping B-line decoding ");
	}

	// Start parsing
	int ival=1;
	
	// Station Component identification 
	int jff; 
	int jzz;
	int job;
	std::string stationType = tokens[ival++];	  
	std::string jff_str = tokens[ival++]; 
	sscanf(jff_str.c_str(),"%80d",&jff);
	std::string jzz_str = tokens[ival++];
	sscanf(jzz_str.c_str(),"%80d",&jzz);
	std::string job_str = tokens[ival++];
	sscanf(job_str.c_str(),"%80d",&job);
	
	// A-line
	float s;
	float z;
	float t;
	float ths;
	float thz;
	float tht;	  
	std::string s_str = tokens[ival++];
	sscanf(s_str.c_str(),"%80f",&s);
	std::string z_str = tokens[ival++];
	sscanf(z_str.c_str(),"%80f",&z);
	std::string t_str = tokens[ival++];
	sscanf(t_str.c_str(),"%80f",&t);
	std::string ths_str = tokens[ival++];
	sscanf(ths_str.c_str(),"%80f",&ths);
	std::string thz_str = tokens[ival++];
	sscanf(thz_str.c_str(),"%80f",&thz);
	std::string tht_str = tokens[ival++];
	sscanf(tht_str.c_str(),"%80f",&tht);

	// B-line
	float bz, bp, bn, sp, sn, tw, pg, tr, eg, ep, en;
	std::string ChamberHwName="";
	  
	if (hasBLine && thisRowHasBLine) {	      
	  std::string tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&bz);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&bp);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&bn);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&sp);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&sn);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&tw);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&pg);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&tr);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&eg);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&ep);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&en);	  

	  // ChamberName (hardware convention)
	  ChamberHwName = tokens[ival++];
	}
	  
	ATH_MSG_VERBOSE("Station type "  << stationType);
	ATH_MSG_VERBOSE("jff / jzz / job "  << jff << " / " << jzz << " / " << job);
	if (hasBLine) {
	  ATH_MSG_VERBOSE(" HardwareChamberName " << ChamberHwName);
	}
	else ATH_MSG_VERBOSE(" ");
	ATH_MSG_VERBOSE("A-line: s,z,t "  << s << " " << z << " " << t);
	ATH_MSG_VERBOSE(" ts,tz,tt "  << ths << " " << thz << " " << tht);
	if (hasBLine) {
	  if (thisRowHasBLine) {
	    ATH_MSG_VERBOSE("B-line:  bz,bp,bn " << bz << " " << bp << " " << bn);
	    ATH_MSG_VERBOSE(" sp,sn " << sp << " " << sn << " tw,pg,tr " << tw << " " << pg << " " << tr
			    << " eg,ep,en " << eg << " " << ep << " " << en);
	  }
	  else ATH_MSG_VERBOSE("No B-line found");
	} 
	  
	int stationName = m_mdtIdHelper->stationNameIndex(stationType);
	Identifier id;
	ATH_MSG_VERBOSE("stationName  " << stationName);
	if (stationType.substr(0,1)=="T") {
	  // tgc case
	  int stPhi = MuonGM::stationPhiTGC(stationType, jff, jzz, m_geometryVersion); // !!!!! The stationPhiTGC implementation in this package is NOT used !!!!!
	  int stEta = 1;
	  if (jzz<0) stEta = -1;			 
	  if (job != 0) {
	    // this should become the default now 
	    stEta=job;
	    if (jzz<0) stEta = -stEta;
	  }
	  id = m_tgcIdHelper->elementID(stationName, stEta, stPhi);
	  ATH_MSG_VERBOSE("identifier being assigned is " << m_tgcIdHelper->show_to_string(id));
	}
	else if (stationType.substr(0,1)=="C") {
	  // csc case
	  id = m_cscIdHelper->elementID(stationName, jzz, jff);
	  ATH_MSG_VERBOSE("identifier being assigned is " << m_cscIdHelper->show_to_string(id));
	}
	else if (stationType.substr(0,3)=="BML" && abs(jzz)==7) {
	  // rpc case
	  id = m_rpcIdHelper->elementID(stationName, jzz, jff, 1);
	  ATH_MSG_VERBOSE("identifier being assigned is " << m_rpcIdHelper->show_to_string(id));
	}
	else {
	  id = m_mdtIdHelper->elementID(stationName, jzz, jff);
	  ATH_MSG_VERBOSE("identifier being assigned is " << m_mdtIdHelper->show_to_string(id));
	}
          

	// new Aline
	++nDecodedLines;
	++nNewDecodedALines;
	ALinePar* newALine = new ALinePar();
	newALine->setAmdbId(stationType, jff, jzz, job);
	newALine->setParameters(s,z,t,ths,thz,tht);
	newALine->isNew(true);	  
	iALineMap ialine;
	if((ialine = writeALineCdo->find(id)) != writeALineCdo->end()) {
	  ATH_MSG_WARNING("More than one (A-line) entry in folder " << folderName << 
			  " for  " << stationType <<
			  " at Jzz/Jff " << jzz << "/" << jff <<
			  " --- keep the latest one");
	  writeALineCdo->erase(id);
	  --nNewDecodedALines;
	}
	writeALineCdo->insert(std::make_pair(id,(ALinePar*)newALine));

	if (hasBLine && thisRowHasBLine) {
	  // new Bline
	  ++nNewDecodedBLines;
	  BLinePar* newBLine = new BLinePar();
	  newBLine->setAmdbId(stationType, jff, jzz, job);
	  newBLine->setParameters(bz, bp, bn, sp, sn, tw, pg, tr, eg, ep, en);
	  newBLine->isNew(true);
	  iBLineMap ibline;
	  if((ibline = writeBLineCdo->find(id)) != writeBLineCdo->end()) {
	    ATH_MSG_WARNING("More than one (B-line) entry in folder " << folderName <<
			    " for  " << stationType <<
			    " at Jzz/Jff " << jzz << "/" << jff <<
			    " --- keep the latest one");
	    writeBLineCdo->erase(id);
	    --nNewDecodedBLines;
	  }
	  writeBLineCdo->insert(std::make_pair(id,(BLinePar*)newBLine));
	}
      }
    }
  }
  ATH_MSG_DEBUG("In folder < " << folderName << 
		  "> # lines/decodedLines/newDecodedALines/newDecodedBLines= " << nLines <<
		  "/" << nDecodedLines <<
		  "/" << nNewDecodedALines <<
		  "/" << nNewDecodedBLines);

  // set A-lines from ASCII file
  if (m_aLinesFile!="" && (int)writeALineCdo->size()>0 ) setALinesFromAscii(writeALineCdo);
  
  // dump A-lines to log file
  if (m_dumpALines && (int)writeALineCdo->size()>0) dumpALines(folderName, writeALineCdo);
   
  // dump B-lines to log file
  if (m_dumpBLines && (int)writeBLineCdo->size()>0) dumpBLines(folderName, writeBLineCdo);

  // !!!!!!!!!!!!!! In the MuonAlignmentDbSvc this was here. I moved it to loadAlignAsBuilt
  // if ( m_asBuiltFile!="" ) setAsBuiltFromAscii();

  ATH_MSG_VERBOSE("Collection CondAttrListCollection CLID " << atrc->clID());
      
  return  StatusCode::SUCCESS; 
}

StatusCode MuonAlignmentDbAlg::loadAlignILines(std::string folderName) 
{

  // =======================
  // Write ILine Cond Handle
  // =======================
  SG::WriteCondHandle<CscInternalAlignmentMapContainer> writeHandle{m_writeILineKey};
  if (writeHandle.isValid()) {
    ATH_MSG_DEBUG("CondHandle " << writeHandle.fullKey() << " is already valid."
		  << ". In theory this should not be called, but may happen"
		  << " if multiple concurrent events are being processed out of order.");
    return StatusCode::SUCCESS;
  }
  std::unique_ptr<CscInternalAlignmentMapContainer> writeCdo{std::make_unique<CscInternalAlignmentMapContainer>()};

  ATH_MSG_INFO("Load alignment parameters from DB folder <" << folderName << ">");

  // =======================
  // Read CSC/ILINES Cond Handle
  // =======================
  SG::ReadCondHandle<CondAttrListCollection> readCscILinesHandle{m_readCscILinesKey};
  const CondAttrListCollection* readCscILinesCdo{*readCscILinesHandle}; 
  if(readCscILinesCdo==nullptr){
    ATH_MSG_ERROR("Null pointer to the read CSC/ILINES conditions object");
    return StatusCode::FAILURE; 
  } 
  
  EventIDRange rangeCscILinesW;
  if ( !readCscILinesHandle.range(rangeCscILinesW) ) {
    ATH_MSG_ERROR("Failed to retrieve validity range for " << readCscILinesHandle.key());
    return StatusCode::FAILURE;
  } 

  ATH_MSG_INFO("Size of CSC/ILINES CondAttrListCollection " << readCscILinesHandle.fullKey() << " readCscILinesCdo->size()= " << readCscILinesCdo->size());
  ATH_MSG_INFO("Range of CSC/ILINES input is " << rangeCscILinesW);

  // =======================
  // Retrieve the collection of strings read out from the DB
  // =======================
  const CondAttrListCollection * atrc;
  if(detStore()->retrieve(atrc,folderName).isFailure())  {
    ATH_MSG_WARNING("could not retreive the CondAttrListCollection from DB folder " << folderName);
    return StatusCode::RECOVERABLE;
  }
  else ATH_MSG_INFO(" CondAttrListCollection from DB folder have been obtained with size " << atrc->size());

  // unpack the strings in the collection and update the 
  // ALlineContainer in TDS
  int nLines = 0;
  int nDecodedLines = 0;
  int nNewDecodedILines = 0;
  CondAttrListCollection::const_iterator itr;
  for (itr = atrc->begin(); itr != atrc->end(); ++itr) {
    const coral::AttributeList& atr=itr->second;
    std::string data;
    data=*(static_cast<const std::string*>((atr["data"]).addressOfData()));
    
    ATH_MSG_DEBUG("Data load is " << data << " FINISHED HERE ");
    
    // Check the first word to see if it is a correction
    std::string type;
    
    //Parse corrections
    std::string since_str;
    std::string till_str;
    std::string delimiter = "\n";
      
    
    std::vector<std::string> lines;
    MuonCalib::MdtStringUtils::tokenize(data,lines,delimiter);
    for (unsigned int i=0; i<lines.size();i++) {
      ++nLines;
      std::string blobline = lines[i];
      
      std::string delimiter = ":";
      std::vector<std::string> tokens;
      MuonCalib::MdtStringUtils::tokenize(blobline,tokens,delimiter);
      type = tokens[0];
      //Parse line
      if (type.find("#")==0) {
	//skip it
	continue;
      }

      if (type.find("Header")==0) {
	std::string delimiter = "|";
	std::vector<std::string> tokens;
	MuonCalib::MdtStringUtils::tokenize(blobline,tokens,delimiter);
	since_str = tokens[1];
	till_str = tokens[2];
      }
	
      if (type.find("IOV")==0) {
	std::string delimiter = " ";
	std::vector<std::string> tokens;
	MuonCalib::MdtStringUtils::tokenize(blobline,tokens,delimiter);
	int ival = 1;
	long int iovThisBlob=0;
	// long int lastIOV = getLastIOVforThisFolder(folderName);

	std::string str_iovThisBlob = tokens[ival];
	sscanf(str_iovThisBlob.c_str(),"%80ld",&iovThisBlob);
	ATH_MSG_INFO("Data read from folder " << folderName <<
		     " have IoV = " << iovThisBlob);
      }
	
      if (type.find("Corr")==0) {
        //# Amdb like clob for ilines using geometry tag ISZT-R06-02 
        //# ISZT_DATA_ID VERS TYP JFF JZZ JOB JLAY TRAS TRAZ TRAT ROTS ROTZ ROTT
        //
        //.... example
        //Corr:  CSL 1 -1 3 1 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000

	std::string delimiter = " ";
	std::vector<std::string> tokens;
	MuonCalib::MdtStringUtils::tokenize(blobline,tokens,delimiter);
	ATH_MSG_VERBOSE("Parsing Line = ");
	for (unsigned int i=0; i<tokens.size(); i++) ATH_MSG_VERBOSE( tokens[i] << " | ");
	ATH_MSG_VERBOSE( " " );

	// Start parsing
	int ival=1;
	
	// Station Component identification 
	int jff; 
	int jzz;
	int job;
	int jlay;
	std::string stationType = tokens[ival++];	  
	std::string jff_str = tokens[ival++]; 
	sscanf(jff_str.c_str(),"%80d",&jff);
	std::string jzz_str = tokens[ival++];
	sscanf(jzz_str.c_str(),"%80d",&jzz);
	std::string job_str = tokens[ival++];
	sscanf(job_str.c_str(),"%80d",&job);
	std::string jlay_str = tokens[ival++];
	sscanf(jlay_str.c_str(),"%80d",&jlay);
	
	// I-line
	float tras;
	float traz;
	float trat;
	float rots;
	float rotz;
	float rott;	
	std::string tras_str = tokens[ival++];
	sscanf(tras_str.c_str(),"%80f",&tras);
	std::string traz_str = tokens[ival++];
	sscanf(traz_str.c_str(),"%80f",&traz);
	std::string trat_str = tokens[ival++];
	sscanf(trat_str.c_str(),"%80f",&trat);
	std::string rots_str = tokens[ival++];
	sscanf(rots_str.c_str(),"%80f",&rots);
	std::string rotz_str = tokens[ival++];
	sscanf(rotz_str.c_str(),"%80f",&rotz);
	std::string rott_str = tokens[ival++];
	sscanf(rott_str.c_str(),"%80f",&rott);

	ATH_MSG_VERBOSE("Station type "  << stationType);
	ATH_MSG_VERBOSE("jff / jzz / job / jlay "  <<jff <<" / "<< jzz<<" / "<< job<<" / "<<jlay);
	ATH_MSG_VERBOSE("I-line: tras,traz,trat "  << tras <<" "<< traz <<" "<< trat);
	ATH_MSG_VERBOSE(" rots,rotz,rott "  << rots <<" "<< rotz <<" "<< rott);
	  
	int stationName = m_cscIdHelper->stationNameIndex(stationType);
	Identifier id;
	ATH_MSG_VERBOSE("stationName  " << stationName);
	if (stationType.substr(0,1)=="C") {
	  // csc case
	  int chamberLayer = 2;
	  if (job != 3) ATH_MSG_WARNING("job = "<<job<<" is not 3 => chamberLayer should be 1 - not existing ! setting 2");
	  id = m_cscIdHelper->channelID(stationType, jzz, jff, chamberLayer, jlay, 0, 1);
	  ATH_MSG_VERBOSE("identifier being assigned is " << m_cscIdHelper->show_to_string(id));
	}
	else {
	  ATH_MSG_ERROR("There is a non CSC chamber in the list of CSC internal alignment parameters.");
	}
	// new Iline
	++nDecodedLines;
	++nNewDecodedILines;
	CscInternalAlignmentPar* newILine = new CscInternalAlignmentPar();
	newILine->setAmdbId(stationType, jff, jzz, job, jlay);
	newILine->setParameters(tras,traz,trat,rots,rotz,rott);
	newILine->isNew(true);	  
	iCscInternalAlignmentMap iiline;
	if((iiline = writeCdo->find(id)) != writeCdo->end()) {
	  ATH_MSG_WARNING("More than one (I-line) entry in folder " << folderName << 
			  " for  " << stationType <<
			  " at Jzz/Jff/Jlay " << jzz << "/" << jff << "/" << jlay <<
			  " --- keep the latest one");
	  writeCdo->erase(id);
	  --nNewDecodedILines;
	}
	//	m_ilineData->insert(std::make_pair(id,(CscInternalAlignmentPar*)newILine));
	writeCdo->insert(std::make_pair(id,(CscInternalAlignmentPar*)newILine));
      }
    }
  }
  ATH_MSG_DEBUG("In folder <" << folderName << "> # lines/decodedLines/newDecodedILines= "
		<<nLines<<"/"<<nDecodedLines<<"/"<<nNewDecodedILines<<"/");

   // dump I-lines to log file TBA
  if (m_dumpILines && (int)writeCdo->size()>0) dumpILines(folderName, writeCdo.get());

  // =======================
  // FIRST update MuonDetectorManager and THEN record the output cond object.
  // =======================

  if (m_muonMgr->updateCSCInternalAlignmentMap(writeCdo.get()).isFailure()) ATH_MSG_ERROR("Unable to updateCSCInternalAlignmentMap" );
  else ATH_MSG_DEBUG("updateCSCInternalAlignmentMap DONE" );

  if (writeHandle.record(rangeCscILinesW, std::move(writeCdo)).isFailure()) {
    ATH_MSG_FATAL("Could not record CscInternalAlignmentMapContainer " << writeHandle.key() 
		  << " with EventRange " << rangeCscILinesW
		  << " into Conditions Store");
    return StatusCode::FAILURE;
  }		  
  ATH_MSG_INFO("recorded new " << writeHandle.key() << " with range " << rangeCscILinesW << " into Conditions Store");
  
  ATH_MSG_VERBOSE("Collection CondAttrListCollection CLID " << atrc->clID());
   
  return  StatusCode::SUCCESS;; 
}

StatusCode MuonAlignmentDbAlg::loadAlignAsBuilt(std::string folderName) 
{

  // =======================
  // Write AsBuilt Cond Handle
  // =======================
  SG::WriteCondHandle<MdtAsBuiltMapContainer> writeHandle{m_writeAsBuiltKey};
  if (writeHandle.isValid()) {
    ATH_MSG_DEBUG("CondHandle " << writeHandle.fullKey() << " is already valid."
		  << ". In theory this should not be called, but may happen"
		  << " if multiple concurrent events are being processed out of order.");
    return StatusCode::SUCCESS;
  }
  std::unique_ptr<MdtAsBuiltMapContainer> writeCdo{std::make_unique<MdtAsBuiltMapContainer>()};

  ATH_MSG_INFO( "Load alignment parameters from DB folder <"<<folderName<<">" );

  // =======================
  // Read MDT/ASBUILTPARAMS Cond Handle
  // =======================
  SG::ReadCondHandle<CondAttrListCollection> readMdtAsBuiltHandle{m_readMdtAsBuiltParamsKey};
  const CondAttrListCollection* readMdtAsBuiltCdo{*readMdtAsBuiltHandle}; 
  if(readMdtAsBuiltCdo==nullptr){
    ATH_MSG_ERROR("Null pointer to the read MDT/ASBUILTPARAMS conditions object");
    return StatusCode::FAILURE; 
  } 
  
  EventIDRange rangeMdtAsBuiltW;
  if ( !readMdtAsBuiltHandle.range(rangeMdtAsBuiltW) ) {
    ATH_MSG_ERROR("Failed to retrieve validity range for " << readMdtAsBuiltHandle.key());
    return StatusCode::FAILURE;
  } 

  ATH_MSG_INFO("Size of MDT/ASBUILTPARAMS CondAttrListCollection " << readMdtAsBuiltHandle.fullKey() << " ->size()= " << readMdtAsBuiltCdo->size());
  ATH_MSG_INFO("Range of MDT/ASBUILTPARAMS input is " << rangeMdtAsBuiltW);

  // =======================
  // Retrieve the collection of strings read out from the DB
  // =======================
  const CondAttrListCollection * atrc;
  if(detStore()->retrieve(atrc,folderName).isFailure())  {
    ATH_MSG_WARNING( "could not retreive the CondAttrListCollection from DB folder " << folderName);
    return StatusCode::RECOVERABLE;
  }
  else ATH_MSG_INFO(" CondAttrListCollection from DB folder have been obtained with size "<< atrc->size());
  
  // unpack the strings in the collection and update the 
  // AsBuiltContainer in TDS
  int nLines = 0;
  int nDecodedLines = 0;
  int nNewDecodedAsBuilt = 0;
  MdtAsBuiltPar xPar;
  xPar.isNew(true);
  CondAttrListCollection::const_iterator itr;
  for (itr = atrc->begin(); itr != atrc->end(); ++itr) {
    const coral::AttributeList& atr=itr->second;
    std::string data;
    data=*(static_cast<const std::string*>((atr["data"]).addressOfData()));
    
    ATH_MSG_DEBUG( "Data load is " << data << " FINISHED HERE " );
    
    // Check the first word to see if it is a correction
    std::string type;
    
    //Parse corrections
    std::string delimiter = "\n";
    
    std::vector<std::string> lines;
    MuonCalib::MdtStringUtils::tokenize(data,lines,delimiter);
    for (unsigned int i=0; i<lines.size();i++) {
      ++nLines;
      std::string blobline = lines[i];
      
      std::string delimiter = ":";
      std::vector<std::string> tokens;
      MuonCalib::MdtStringUtils::tokenize(blobline,tokens,delimiter);
      type = tokens[0];
      //Parse line
      if (type.find("#")==0) {
         //skip it
         continue;
      }
	
      if (type.find("Corr")==0) {
	if (!xPar.setFromAscii(blobline)) {
          ATH_MSG_ERROR( "Unable to parse AsBuilt params from Ascii line: " << blobline  );
          continue;
	}

        std::string stationType="XXX";
        int jff = 0;
        int jzz = 0;
        int job = 0;
        xPar.getAmdbId(stationType, jff, jzz, job);
	Identifier id = m_mdtIdHelper->elementID(stationType, jzz, jff);
	
	ATH_MSG_VERBOSE("Station type jff jzz "  << stationType  << jff << " " << jzz  );
        ++nDecodedLines;
        ++nNewDecodedAsBuilt;
        ciMdtAsBuiltMap iasbuild;
        if((iasbuild = writeCdo->find(id)) != writeCdo->end()) {
	  ATH_MSG_WARNING( "More than one (As-built) entry in folder "<<folderName<<" for  "
                           << stationType<<" at Jzz/Jff "<<jzz<<"/"<< jff<<" --- keep the latest one" );
          writeCdo->erase(id);
          --nNewDecodedAsBuilt;
        }
	writeCdo->insert(std::make_pair(id,new MdtAsBuiltPar(xPar)));
      }
    }
  }
  ATH_MSG_DEBUG("In folder <"<<folderName<<"> # lines/decodedLines/newDecodedILines= "
		<<nLines<<"/"<<nDecodedLines<<"/"<<nNewDecodedAsBuilt<<"/" );

  // !!!!!!!!!!!!!! In the MuonAlignmentDbTool this was in loadAlignABLines. I moved it here
  if ( m_asBuiltFile!="" ) setAsBuiltFromAscii(writeCdo.get());

  // =======================
  // FIRST update MuonDetectorManager and THEN record the output cond object.
  // =======================
  if (m_muonMgr->updateAsBuiltParams(writeCdo.get()).isFailure()) ATH_MSG_ERROR("Unable to updateAsBuiltParams" );
  else ATH_MSG_DEBUG("updateAsBuiltParams DONE" );

  if (writeHandle.record(rangeMdtAsBuiltW, std::move(writeCdo)).isFailure()) {
    ATH_MSG_FATAL("Could not record MdtAsBuiltMapContainer " << writeHandle.key() 
		  << " with EventRange " << rangeMdtAsBuiltW
		  << " into Conditions Store");
    return StatusCode::FAILURE;
  }		  
  ATH_MSG_INFO("recorded new " << writeHandle.key() << " with range " << rangeMdtAsBuiltW << " into Conditions Store");

  ATH_MSG_VERBOSE( "Collection CondAttrListCollection CLID " << atrc->clID() );
  
  return  StatusCode::SUCCESS;; 
}

void MuonAlignmentDbAlg::dumpALines(const std::string& folderName,
				    ALineMapContainer* writeALineCdo)
{

  ATH_MSG_INFO("dumping A-lines for folder " << folderName);
  ATH_MSG_INFO("A type jff jzz job s(cm)  z(cm)  t(cm)  ths(rad)  thz(rad)  tht(rad)  ID");
  
  for (std::map<Identifier,ALinePar*>::const_iterator cialine = writeALineCdo->begin(); 
       cialine != writeALineCdo->end(); ++cialine) {
    Identifier ALineId = (*cialine).first;
    ALinePar* ALine = (*cialine).second;
    std::string stationType;
    int jff,jzz,job;
    ALine->getAmdbId(stationType,jff,jzz,job);
    float s,z,t,ths,thz,tht;
    ALine->getParameters(s,z,t,ths,thz,tht);
    
    ATH_MSG_INFO("A " << std::setiosflags(std::ios::fixed|std::ios::right)
		 << std::setw(4) << stationType  <<" " 
		 << std::setw(2) << jff  <<"  " 
		 << std::setw(2) << jzz  <<" " 
		 << std::setw(2) << job  <<" "
		 << std::setw(6) << std::setprecision(3) <<  s   <<" "  // here cm !
		 << std::setw(6) << std::setprecision(3) <<  z   <<" "  // here cm !
		 << std::setw(6) << std::setprecision(3) <<  t   <<" "  // here cm !
		 << std::setw(6) << std::setprecision(6) << ths  <<" " 
		 << std::setw(6) << std::setprecision(6) << thz  <<" " 
		 << std::setw(6) << std::setprecision(6) << tht  <<" "
		 << ALineId);

  }
  //std::cout<<std::endl;
}

void MuonAlignmentDbAlg::dumpBLines(const std::string& folderName,
				    BLineMapContainer* writeBLineCdo)
{

  ATH_MSG_INFO( "dumping B-lines for folder " << folderName);
  ATH_MSG_INFO( "B type jff jzz job bs       bp        bn        sp        sn        tw        pg        tr        eg        ep        en        ID");
  
  for (std::map<Identifier,BLinePar*>::const_iterator cibline = writeBLineCdo->begin(); 
       cibline != writeBLineCdo->end(); ++cibline) {
    Identifier BLineId = (*cibline).first;
    BLinePar* BLine = (*cibline).second;
    std::string stationType;
    int jff,jzz,job;
    BLine->getAmdbId(stationType,jff,jzz,job);
    float bs,bp,bn,sp,sn,tw,pg,tr,eg,ep,en;
    BLine->getParameters(bs,bp,bn,sp,sn,tw,pg,tr,eg,ep,en);
    
    ATH_MSG_INFO( "B " << std::setiosflags(std::ios::fixed|std::ios::right)
		  << std::setw(4) << stationType  <<" " 
		  << std::setw(2) << jff  <<" " 
		  << std::setw(2) << jzz  <<" " 
		  << std::setw(2) << job  <<"  "
		  << std::setw(6) << std::setprecision(6) <<  bs  <<" "
		  << std::setw(6) << std::setprecision(6) <<  bp  <<" "
		  << std::setw(6) << std::setprecision(6) <<  bn  <<" "
		  << std::setw(6) << std::setprecision(6) <<  sp  <<" " 
		  << std::setw(6) << std::setprecision(6) <<  sn  <<" " 
		  << std::setw(6) << std::setprecision(6) <<  tw  <<" "
		  << std::setw(6) << std::setprecision(6) <<  pg  <<" "
		  << std::setw(6) << std::setprecision(6) <<  tr  <<" "
		  << std::setw(6) << std::setprecision(6) <<  eg  <<" "
		  << std::setw(6) << std::setprecision(6) <<  ep  <<" "
		  << std::setw(6) << std::setprecision(6) <<  en  <<" "
		  << BLineId);
  }
}

void MuonAlignmentDbAlg::dumpILines(const std::string& folderName, 
				    CscInternalAlignmentMapContainer* writeCdo)
{

  ATH_MSG_INFO( "dumping I-lines for folder " << folderName);
  ATH_MSG_INFO( "I \ttype\tjff\tjzz\tjob\tjlay\ttras\ttraz\ttrat\trots\trotz\trott");
  
  for (std::map<Identifier,CscInternalAlignmentPar*>::const_iterator ciiline = writeCdo->begin(); 
       ciiline != writeCdo->end(); ++ciiline) {
    Identifier ILineId = (*ciiline).first;
    CscInternalAlignmentPar* ILine = (*ciiline).second;
    std::string stationType;
    int jff,jzz,job,jlay;
    ILine->getAmdbId(stationType,jff,jzz,job,jlay);
    float tras,traz,trat,rots,rotz,rott;
    ILine->getParameters(tras,traz,trat,rots,rotz,rott);
    
    ATH_MSG_INFO( "I\t" << std::setiosflags(std::ios::fixed|std::ios::right)
		  << std::setw(4) << stationType  <<"\t" 
		  << std::setw(2) << jff  <<"\t" 
		  << std::setw(2) << jzz  <<"\t" 
		  << std::setw(2) << job  <<"\t"
		  << std::setw(2) << jlay  <<"\t"
		  << std::setw(6) << std::setprecision(6) <<  tras  <<"\t"
		  << std::setw(6) << std::setprecision(6) <<  traz  <<"\t"
		  << std::setw(6) << std::setprecision(6) <<  trat  <<"\t"
		  << std::setw(6) << std::setprecision(6) <<  rots  <<"\t" 
		  << std::setw(6) << std::setprecision(6) <<  rotz  <<"\t" 
		  << std::setw(6) << std::setprecision(6) <<  rott  <<"\t"
		  << ILineId);
  }
  //std::cout<<std::endl;
}

void MuonAlignmentDbAlg::setALinesFromAscii(ALineMapContainer* writeALineCdo) const
{

  ATH_MSG_INFO( " Set alignment constants from text file " << m_aLinesFile);

  std::ifstream infile;
  infile.open(m_aLinesFile.c_str());
  
  char line[512] ;
  ATH_MSG_DEBUG( "reading file");
  
  while( infile.getline(line,512) ) {
    
    std::istringstream is(line) ;
    
    char AlineMarker[2];
    std::string name; int jff,jzz,obj;
    float tras,traz,trat,rots,rotz,rott;
    is >> AlineMarker >> name >> jff >> jzz >> obj
       >> tras >> traz >> trat >> rots >> rotz >> rott;
    
    ATH_MSG_DEBUG( "read line: " << line);
    
    if( AlineMarker[0] == '\0') {
      ATH_MSG_DEBUG( "read empty line!"); 
    }
    else {
      
      // loop through A-line container and find the correct one
      std::string testStationType;
      int testJff,testJzz,testJob;
      ALinePar* ALine(0);
      for (std::map<Identifier,ALinePar*>::const_iterator cialine = writeALineCdo->begin(); 
	   cialine != writeALineCdo->end(); ++cialine) {
	
	(*cialine).second->getAmdbId(testStationType,testJff,testJzz,testJob);
	if (testStationType==name &&
	    testJff        == jff &&
	    testJzz        == jzz) {
	  ALine = (*cialine).second;
	  break;
	}
      }

      // set parameter if you found it
      if (ALine) 
	ALine->setParameters(tras,traz,trat,rots,rotz,rott);      
    }
  }
  return;
}

void MuonAlignmentDbAlg::setAsBuiltFromAscii(MdtAsBuiltMapContainer* writeCdo) const
{
  ATH_MSG_INFO (" Set alignment constants from text file "<< m_asBuiltFile);

  std::ifstream fin(m_asBuiltFile.c_str());
  std::string line;
  MdtAsBuiltPar xPar;
  xPar.isNew(true);
  int count = 0;
  while(getline(fin, line)) {
    if (line.find("Corr:")==0) {
      if (!xPar.setFromAscii(line)) {
        ATH_MSG_ERROR( "Unable to parse AsBuilt params from Ascii line: " << line  );
	} else {
        std::string stName="XXX";
        int jff = 0;
        int jzz = 0;
        int job = 0;
        xPar.getAmdbId(stName, jff, jzz, job);
        Identifier id = m_mdtIdHelper->elementID(stName, jzz, jff);
        if (!id.is_valid()) {
          ATH_MSG_ERROR( "Invalid MDT identifiers: sta=" << stName << " eta=" << jzz << " phi=" << jff  );
           continue;
        }
        std::map<Identifier,MdtAsBuiltPar*>::iterator ci = writeCdo->begin();
        if((ci = writeCdo->find(id)) != writeCdo->end())
        {
          ATH_MSG_DEBUG( "Updating extisting entry in AsBuilt container for Station " <<stName<<" at Jzz/Jff "<<jzz<<"/"<< jff  );
          ATH_MSG_DEBUG( "That is strange since it's read from ASCII so this station is listed twice!"  );
          MdtAsBuiltPar* oldAsBuilt =  (*ci).second;
          writeCdo->erase(id);
          delete oldAsBuilt; oldAsBuilt=0;
        } else {
          ATH_MSG_DEBUG( "New entry in AsBuilt container for Station "
                         <<stName<<" at Jzz/Jff "<<jzz<<"/"<< jff<<" --- in the container with key "<< m_mdtIdHelper->show_to_string(id) );
        }
	  writeCdo->insert(std::make_pair(id,new MdtAsBuiltPar(xPar)));
	  ++count;
	}
    }
  }
  ATH_MSG_INFO( "Parsed AsBuilt parameters: " << count  );

  return;
}
